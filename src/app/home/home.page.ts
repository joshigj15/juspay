import { Component } from '@angular/core';

declare var ExpressCheckout;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

  payWithJuspay() {
//     console.log(123456);
    
    var nbPayload = {
      opName: 'nbTxn',
      paymentMethodType: "NB",
      paymentMethod: "NB_DUMMY", // Eg: NB_SBI
      redirectAfterPayment: "true",
      format: "json"
    }
    var requestPayload = {
      baseParams: {
        merchant_id: "farmery",
        client_id: "farmery_android",
        currency:'INR',
        order_id: "457",
        amount: "5000.00", //eg: "1.00"
        customer_id : "123",
        customer_email : "joshigj15@gmail.com",
        customer_phone_number : "9462305079",
        environment: "sandbox" //eg: "sandbox" or "prod"
      },
      serviceParams: {
        service: "in.juspay.ec",
        session_token: "B24D05DFDC34B4E80AE9E9401DF4E8",
        end_urls: ["https://sandbox.juspay.in"], //eg: ["https://www.reload.in/recharge/", ".*www.reload.in/payment/f.*"]
        payload: JSON.stringify(nbPayload)
      },
      customParams: {}, //customParams are optional key value pairs. { udf_circle: "Andhra Pradesh" }
      onSuccess: function (successResponse) {
        alert("Success Response ==>"+ JSON.stringify(successResponse));
      },
      onError: function (errorResponse) {
        alert("Error Response ==>"+ JSON.stringify(errorResponse));
      }
};
var expressCheckout = new ExpressCheckout();
// var orderId = "457"
// var merchantId = "farmery"
// var abortedCallback = function () {
//   alert("transation abort by user");
// };
// var onEndUrlReached = function () {
//   alert("paymrnt success");
// };
// var endUrls = ["https://www.farmery.in/"];
// expressCheckout.startPayment({
//   "endUrls": endUrls,
//   "onEndUrlReached": onEndUrlReached,
//   "onTransactionAborted": abortedCallback,
//   "environment": "SANDBOX", // can be either of "SANDBOX" or "PRODUCTION"
//   "instruments": ["CARD", "NB", "WALLET"],
//   "parameters": {
//       "orderId": orderId,
//       "merchantId": merchantId,
//   },
// });
expressCheckout.startPayment(requestPayload);
  }

}
